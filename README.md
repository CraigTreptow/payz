# payz

## Dependincies

### Specifying dependencies

```
python -m pip install flake8
python -m pip install autopep8
python -m pip install pyling
python -m pip install mypy
python -m pip install behave
python -m pip freeze > requirements.txt
```

### Installing dependencies

`pip install -r requirements.txt`

## Tests

- Unit and functional testing using `pytest`
- Behavioral testing using `behave`
- Linting using `flake8`
- Static analysis using `pylint`
- Type checking using `mypy`

### Running tests

`poetry run behave`
