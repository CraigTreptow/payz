from payz.toople import Toople


class TestTooples:
    def test_some_tooples_are_points(self) -> None:
        a = Toople(x=4.3, y=-4.2, z=3.1, w=1.0)

        assert a.x == 4.3
        assert a.y == -4.2
        assert a.z == 3.1
        assert a.w == 1.0
        assert a.is_point()
        assert not a.is_vector()

    def test_some_tooples_are_vectors(self) -> None:
        a = Toople(x=4.3, y=-4.2, z=3.1, w=0.0)

        assert a.x == 4.3
        assert a.y == -4.2
        assert a.z == 3.1
        assert a.w == 0.0
        assert not a.is_point()
        assert a.is_vector()

    def test_can_create_a_point(self) -> None:
        p = Toople.create_point(x=4.0, y=-4.0, z=3.0)
        assert p.x == 4.0
        assert p.y == -4.0
        assert p.z == 3.0
        assert p.w == 1.0
        assert p.is_point()
        assert not p.is_vector()

    def test_can_create_a_vector(self) -> None:
        v = Toople.create_vector(x=4.0, y=-4.0, z=3.0)
        assert v.x == 4.0
        assert v.y == -4.0
        assert v.z == 3.0
        assert v.w == 0.0
        assert not v.is_point()
        assert v.is_vector()
